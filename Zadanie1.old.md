# Zadanie 1 
Dane: [Sports on Stackexchange](https://archive.org/download/stackexchange/sports.stackexchange.com.7z)

Dane zawierają informacje o postach dotyczących sportu z portalu stackexchange.com.
W zadaniu wykorzystano informacje o użytkownikach i o napisanych przez nich postach.

| Dane | Liczba rekordów |
|------|---------------|
| users | 6423 |
| posts | 9366 |

## MongoDB

Dane zostały przekonwertowane z formatu xml na json.

### Import

**Użytkownicy:**
```
powershell (Measure-Command {mongoimport -c  users --file '.\Dane\Users.json' --jsonArray}).TotalSeconds
```
Czas importu: 0,8118943 s

- - -

**Posty:**
```
powershell (Measure-Command {mongoimport -c  posts --file '.\Dane\Posts.json' --jsonArray}).TotalSeconds
```
Czas importu: 1,6398761 s

### Obróbka zaimportowanych danych
Część pól dla kolekcji *posts* i *posts* należało przekonwertować.

**users:**

| pole | typ |
|------|-----|
| _Id | int |
| _Reputation | int |
| _Views | int |
| _UpVotes | int |
| _DownVotes | int |
| _AccountId | int |
| _CreationDate | date |
| _LastAccessDate | date |

**Konwersja pól kolekcji *users*:**
```
db.users.find().forEach( x => { x._Id = parseInt(x._Id); db.users.save(x);})
db.users.find().forEach( x => { x._Reputation = parseInt(x._Reputation); db.users.save(x);})
db.users.find().forEach( x => { x._Views = parseInt(x._Views); db.users.save(x);})
db.users.find().forEach( x => { x._UpVotes = parseInt(x._UpVotes); db.users.save(x);})
db.users.find().forEach( x => { x._DownVotes = parseInt(x._DownVotes); db.users.save(x);})
db.users.find().forEach( x => { x._AccountId = parseInt(x._AccountId); db.users.save(x);})

db.users.find().forEach( x => { x._CreationDate = new Date(x._CreationDate); db.users.save(x);})
db.users.find().forEach( x => { x._LastAccessDate = new Date(x._LastAccessDate); db.users.save(x);})
```

- - -

**posts:**

| pole | typ |
|------|-----|
| _Id | int |
| _PostTypeId | int |
| _AcceptedAnswerId | int |
| _Score | int |
| _ViewCount | int |
| _OwnerUserId | int |
| _LastEditorUserId | int |
| _AnswerCount | int |
| _CommentCount | int |
| _FavoriteCount | int |
| _CreationDate | date |
| _LastEditDate | date |
| _LastActivityDate | date |

**Konwersja pól kolekcji *posts*:**
```
db.posts.find().forEach( x => { x._Id = parseInt(x._Id); db.posts.save(x);})
db.posts.find().forEach( x => { x._PostTypeId = parseInt(x._PostTypeId); db.posts.save(x);})
db.posts.find().forEach( x => { x._AcceptedAnswerId = parseInt(x._AcceptedAnswerId); db.posts.save(x);})
db.posts.find().forEach( x => { x._Score = parseInt(x._Score); db.posts.save(x);})
db.posts.find().forEach( x => { x._ViewCount = parseInt(x._ViewCount); db.posts.save(x);})
db.posts.find().forEach( x => { x._OwnerUserId = parseInt(x._OwnerUserId); db.posts.save(x);})
db.posts.find().forEach( x => { x._LastEditorUserId = parseInt(x._LastEditorUserId); db.posts.save(x);})
db.posts.find().forEach( x => { x._AnswerCount = parseInt(x._AnswerCount); db.posts.save(x);})
db.posts.find().forEach( x => { x._CommentCount = parseInt(x._CommentCount); db.posts.save(x);})
db.posts.find().forEach( x => { x._FavoriteCount = parseInt(x._FavoriteCount); db.posts.save(x);})

db.posts.find().forEach( x => { x._CreationDate = new Date(x._CreationDate); db.posts.save(x);})
db.posts.find().forEach( x => { x._LastEditDate = new Date(x._LastEditDate); db.posts.save(x);})
db.posts.find().forEach( x => { x._LastActivityDate = new Date(x._LastActivityDate); db.posts.save(x);})
```

### Agregacja nr 1: 3 posty z najwyższym _Score
```
db.posts.aggregate([  
    {$sort: {_Score: -1}},  
    {$limit: 3},  
    {$project: {_id: 0, _Id: 1, _Score : 1}}   
]).pretty()
```

| _Id | Score |
|-----|-------|
| 203 | 74 |
| 179 | 65 |
| 205 | 60 |

Czas wykonania: 12 ms

### Agregacja nr 2: 3 ostatnio napisane posty
```
db.posts.aggregate([
    {$project: {_id: 0, _Id: 1, _CreationDate: 1}},
	{$sort: {_CreationDate: -1}},
    {$limit: 3}
]).pretty()
```
| _Id | Date |
|-----|------|
| 15702 | ISODate("2017-03-13T18:42:31.757Z") |
| 15701 | ISODate("2017-03-13T16:16:45.167Z") |
| 15699 | ISODate("2017-03-13T10:45:13.800Z") |

Czas wykonania: 56 ms

### Agregacja nr 3: Użytkownicy z liczbą postów między 5 a 7
```
db.posts.aggregate([
	{$lookup: {from: "users", localField: "_OwnerUserId", foreignField: "_AccountId", as: "X"}},  
	{$group: {_id: "$X._AccountId", posts: { $sum: 1 }}},   
	{$match : {posts: {$gte: 5, $lte: 7}}},  
	{$sort: {posts: -1 }}, 
    {$project : {posts: 1}}
]).pretty()
```

| _AccountId | Liczba postów |
|------------|---------------|
|10836 | 6 |

Czas wykonania 45.2 s

### Podsumowanie:

| Nr agregacji | Czas agregacji |
|--------------|----------------|
| 1 | 12 ms |
| 2 | 56 ms |
| 3 | 45.2 s |


## PostgreSQL

### Utworzenie schematów tabel

**Tabela users:**
```
CREATE TABLE users(
	_Id BIGINT,
    _Reputation BIGINT,
    _CreationDate TIMESTAMP,
    _DisplayName VARCHAR(255),
    _LastAccessDate TIMESTAMP,
    _WebsiteUrl VARCHAR(255),
    _Location VARCHAR(255),
    _Views BIGINT,
    _UpVotes BIGINT,
    _DownVotes BIGINT,
    _AccountId BIGINT
);
```

**Tabela posts:**
```
CREATE TABLE POSTS(
	_Id BIGINT,
    _PostTypeId INT,
    _AcceptedAnswerId INT,
    _CreationDate TIMESTAMP,
    _Score BIGINT,
    _ViewCount BIGINT,
    _OwnerUserId BIGINT,
    _LastEditorUserId BIGINT,
    _LastEditDate TIMESTAMP,
    _LastActivityDate TIMESTAMP,
    _Title VARCHAR(500),
    _Tags VARCHAR(500),
    _AnswerCount BIGINT,
    _CommentCount BIGINT,
    _FavoriteCount BIGINT
);
```

### Import danych do bazy PostgreSQL

Dane zostały przekonwertowane do formatu CSV.
Pomiar czasu wykonany przy wykorzystaniu przełącznika *\timing*

**Import użytkowników:**
*(polecenie wywołane wewnątrz konsoli psql)*
```
\copy users FROM 'D:\Dane\Users.csv' DELIMITER ';' CSV HEADER
```
Czas trwania: 102.712 ms

**Import użytkowników:**
*(polecenie wywołane wewnątrz konsoli psql)*
```
\copy posts FROM 'D:\Dane\Posts.csv' DELIMITER ';' CSV HEADER
```
Czas trwania: 127.737 ms

### Zapytanie nr 1: 3 posty z najwyższym _Score
```
SELECT _id, _score FROM posts ORDER BY _score DESC LIMIT 3;
```

| _Id | Score |
|-----|-------|
| 203 | 74 |
| 179 | 65 |
| 205 | 60 |

Czas trwania: 412 ms


### Zapytanie nr 2: 3 ostatnio napisane posty
```
SELECT _id, _creationdate FROM posts ORDER BY _creationdate DESC LIMIT 3;
```

| _Id | Data |
|-----|------|
| 15702 | 2017-03-13 18:42:31.757 |
| 15701 | 2017-03-13 16:16:45.167 |
| 15699 | 2017-03-13 10:45:13.800 |

Czas trwania: 910 ms

### Zapytanie nr 3: Użytkownicy z liczbą postów między 5 a 7
```
SELECT u._accountid, COUNT(*) FROM users u JOIN posts p 
ON u._accountid = p._owneruserid GROUP BY u._accountid HAVING COUNT(*) BETWEEN 5 AND 7;
```

| _AccountId | Liczba postów |
|------------|---------------|
|10836 | 6 |

Czas wykonania 8.589 ms

### Podsumowanie:

| Nr zapytania | Czas zapytania |
|--------------|----------------|
| 1 | 412 ms |
| 2 | 910 ms |
| 3 | 8.589 ms  |


# Zestawienie wyników baz mongoDB i PostgreSQL

## Porównanie czasu importu danych do bazy

| Tabela/kolekcja | mongoDB | PostgreSQL |
|-----------------|---------|------------|
| users | 811.894 ms | 102.712 ms |
| posts | 1,64 s | 127.737 ms |

## Porównanie czasu wykonania zapytań/agregacji

| nr | mongoDB | PostgreSQL |
|----|---------|------------|
| 1  | 12 ms | 412 ms |
| 2  | 56 ms | 910 ms |
| 3  | 45.2 s | 8.589 ms|

### [Powrót do strony głównej](https://bitbucket.org/nosql2017/nosql/wiki/Home)